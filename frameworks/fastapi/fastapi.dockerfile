FROM python:3.7-buster
RUN apt update && apt-get install -y netcat
RUN pip install fastapi uvicorn databases[postgresql] orjson
WORKDIR /app
COPY ./wait-for.sh /app/wait-for.sh