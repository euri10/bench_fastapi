import json
import logging

import asyncpg
import orjson
import typing
import uvicorn
from asyncpg import Connection, PostgresConnectionError, SyntaxOrAccessError
from databases import Database
from fastapi import Depends, FastAPI, HTTPException
from starlette.requests import Request
from starlette.responses import JSONResponse
from starlette.status import HTTP_500_INTERNAL_SERVER_ERROR

app = FastAPI()
# debug locally with
# docker run -d -p 5438:5432 --name=bfapi.db bfapi.db
# docker exec -t bfapi.db pg_restore -U postgres -d f1db /data/f1db.dump
# database_url = "postgresql://postgres:postgres@localhost:5438/f1db?search_path=f1db"
database_url = "postgresql://postgres:postgres@bfapi.db/f1db?search_path=f1db"
database = Database(database_url)
database_pool = None

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class ORJSONResponse(JSONResponse):
    media_type = "application/json"

    def render(self, content: typing.Any) -> bytes:
        return orjson.dumps(content)


async def get_db(request: Request) -> Connection:
    try:
        conn: Connection = await database_pool.acquire()
        try:
            await conn.fetch("SELECT 1")
        except asyncpg.ConnectionDoesNotExistError:
            conn = await database_pool.acquire()
        request.state.db = conn
        return conn
    except (PostgresConnectionError, OSError) as e:
        logger.error("Unable to connect to the database: %s", e)
        raise HTTPException(
            detail="Unable to connect to the database.",
            status_code=HTTP_500_INTERNAL_SERVER_ERROR,
        )
    except SyntaxOrAccessError as e:
        logger.error("Unable to execute query: %s", e)
        raise HTTPException(
            detail="Unable to execute the required query to obtain data from the database.",
            status_code=HTTP_500_INTERNAL_SERVER_ERROR,
        )


@app.on_event("startup")
async def startup():
    await database.connect()
    global database_pool
    database_pool = await asyncpg.create_pool(database_url)
    logger.debug("started")


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()
    await database_pool.close()
    logger.debug("started")


@app.middleware("http")
async def middleware(request: Request, call_next):
    try:
        return await call_next(request)
    finally:
        if hasattr(request.state, "db"):
            await database_pool.release(request.state.db)


@app.get("/root")
async def root():
    return {"fastapi": "root"}


@app.get("/circuits")
async def circuits():
    query = """select name, location, country from circuits"""
    response = await database.fetch_all(query)
    return response


@app.get("/circuits_json_in_postgresql")
async def circuits_json_in_postgresql(db: asyncpg.Connection = Depends(get_db)):
    query = """select coalesce(array_to_json(array_agg(row_to_json(response))),'[]')::character varying AS BODYFROM from (
       select name, location, country from circuits order by location
    ) response"""
    response = await db.fetchrow(query)
    return JSONResponse(json.loads(response.get("bodyfrom")))


@app.get("/circuits_json_in_postgresql_orjson")
async def circuits_json_in_postgresql_orjson(db: asyncpg.Connection = Depends(get_db)):
    query = """select coalesce(array_to_json(array_agg(row_to_json(response))),'[]')::character varying AS BODYFROM from (
       select name, location, country from circuits order by location
    ) response"""
    response = await db.fetchrow(query)
    return ORJSONResponse(orjson.loads(response.get("bodyfrom")))


if __name__ == "__main__":
    uvicorn.run("main:app", reload=True)
