from pathlib import Path
from typing import Any, Dict, Optional

from docker.models.images import Image
from pydantic import BaseModel


class CommonConfig(BaseModel):
    directory: Path
    path: str
    image: Image
    name: str
    container_name: str
    config: Optional[Dict[Any, Any]]
    volume: Optional[str]

    class Config:
        arbitrary_types_allowed = True
