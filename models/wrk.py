from typing import Optional, Union

from pydantic import BaseModel


class LatencyResult(BaseModel):
    latency_avg: float
    Ulatency_avg: str
    latency_stdev: float
    Ulatency_stdev: str
    latency_max: float
    Ulatency_max: str
    latency_plusminus: float
    Ulatency_plusminus: str


class ReqPerSecResult(BaseModel):
    rps_avg: float
    Urps_avg: str
    rps_stdev: float
    Urps_stdev: str
    rps_max: float
    Urps_max: str
    rps_plusminus: float
    Urps_plusminus: str


class WrkError(BaseModel):
    connect: int
    read: int
    write: int
    timeout: int


class NonXError(BaseModel):
    nonX: int


class WrkResult(BaseModel):
    duration: int
    Uduration: str
    endpoint: str
    threads: int
    connections: int
    latencyResults: LatencyResult
    reqPerSecResults: ReqPerSecResult
    reqtot: int
    timetot: float
    Utimetot: str
    sizetot: float
    Usizetot: str
    wrk_error: Optional[Union[WrkError, NonXError]]
    rps: float
    tps: float
    Utps: str
