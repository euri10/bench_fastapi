#!make

app_root = .

pkg_src =  $(app_root)

tests_src = $(pkg_src)/tests

isort = isort --multi-line=3 --trailing-comma --force-grid-wrap=0 --combine-as --line-width 88 --recursive --apply $(pkg_src) $(tests_src)

black = black $(pkg_src) $(tests_src)

flake8 = flake8 --config=$(app_root)/.flake8 $(pkg_src)

mypy = mypy --config-file $(app_root)/mypy.ini $(pkg_src)

.PHONY: format
format:
	$(isort)
	$(black)

.PHONY: check-format
check-format:
#	$(isort) --check-only --diff
	$(black) --check

.PHONY: lint
lint:
	$(flake8)

.PHONY: mypy
mypy:
	$(mypy)

.PHONY: test
test:
	PYTHONPATH=$(app_root) pytest -sv $(tests_src) --log-cli-level=debug

.PHONY: dev
dev:
	docker-compose -f docker-compose-dev.yml build
	docker-compose -f docker-compose-dev.yml down -v --remove-orphans
	docker-compose -f docker-compose-dev.yml up -d
	sleep 3
	docker exec -t bfapi.db pg_restore -U postgres -d f1db /data/f1db.dump