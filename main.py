import errno
import logging
import os
import time
import uuid
from pathlib import Path
from typing import Any, Dict, List, Optional

import docker
import yaml
from docker import DockerClient
from docker.errors import NotFound
from docker.models.containers import Container
from docker.models.networks import Network

from models.bench import CommonConfig
from utils import parsed_results

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class Config(object):
    def __init__(self, directory: Path = Path("."), bench_filename: str = "bench.yaml"):
        self.client: DockerClient = docker.from_env()
        self.network: Network = self.setup_network("benchnetwork")
        self.benchmarks = self.setup_config_benchmarks(
            bench_filename=directory / bench_filename
        )
        self.frameworks: Dict[str, CommonConfig] = self.setup_config_frameworks(
            buildpath=directory / "frameworks"
        )
        self.db: CommonConfig = self.setup_config_common(
            buildpath=directory / "common/db"
        )
        self.wrk: CommonConfig = self.setup_config_common(
            buildpath=directory / "common/wrk"
        )

    def setup_config_frameworks(self, buildpath: Path) -> Dict[str, CommonConfig]:
        results = {}
        assert buildpath.exists()
        for directory in buildpath.iterdir():
            path = directory.as_posix()
            name = directory.name
            container_name = f"bfapi.{name}"
            image = self.client.images.build(
                path=path, dockerfile=f"{name}.dockerfile", tag=f"bfapi.{name}"
            )
            if (directory / f"{name}.yaml").exists():
                with open((directory / f"{name}.yaml").as_posix(), "r") as stream:
                    config = yaml.load(stream, yaml.FullLoader)
                volume = (
                    (directory.absolute()).as_posix()
                    if config.get("use_volume")
                    else None
                )
                result = CommonConfig(
                    directory=directory,
                    path=path,
                    image=image[0],
                    name=name,
                    container_name=container_name,
                    config=config,
                    volume=volume,
                )
            results[name] = result
        return results

    def setup_config_common(self, buildpath: Path) -> CommonConfig:
        path = buildpath.as_posix()
        name = buildpath.name
        container_name = f"bfapi.{name}"
        image = self.client.images.build(
            path=path, dockerfile=f"{name}.dockerfile", tag=f"bfapi.{name}"
        )
        result = CommonConfig(
            directory=path,
            path=path,
            image=image[0],
            name=name,
            container_name=container_name,
        )
        return result

    def cleanup_network(self) -> bool:
        try:
            networks_list = self.client.networks.list()
            for network in networks_list:
                if network.name == self.network.name:
                    network.remove()
        except Exception as e:
            logger.debug(e)
            return False
        return True

    def setup_network(self, networkName: str) -> Network:
        try:
            network = self.client.networks.get(networkName)
        except NotFound as e:
            logger.error(e)
            network = self.client.networks.create(networkName)
        return network

    # TODO : better mypy return ?
    def setup_config_benchmarks(self, bench_filename: Path) -> Any:
        bench = Path(bench_filename)
        if bench.exists():
            with open(bench.as_posix(), "r") as stream:
                config = yaml.load(stream, yaml.FullLoader)
            return config
        else:
            logger.error(f"missing {bench_filename} config file")
            raise FileNotFoundError(
                errno.ENOENT, os.strerror(errno.ENOENT), bench.as_posix()
            )

    @property
    def all_containers(self) -> List[str]:
        result = [f.container_name for f in self.frameworks.values()]
        result.append(self.wrk.container_name)
        result.append(self.db.container_name)
        return result


class Bfapi(object):
    def __init__(self, config: Config):
        self.config = config

    def run(self) -> None:
        try:
            db_started = self.start(self.config.db, kill_if_up=False)
            logger.info(f"Launched db {db_started.name} {db_started}")
            time.sleep(5)
            (db_restore_exit_code, output) = db_started.exec_run(
                cmd="pg_restore -U postgres -v -d f1db /data/f1db.dump"
            )
            if db_restore_exit_code == 0:
                logger.debug(f"Restored f1db: {output.decode()}")
            else:
                logger.error(f"Restore error : {output.decode()}")
            framework: str
            framework_config: CommonConfig
            wrk_container: Container = self.start(container_config=self.config.wrk)
            for framework, framework_config in self.config.frameworks.items():
                logger.info(f"Launching container {framework_config.container_name}")
                framework_container = self.start(container_config=framework_config)
                logger.info(
                    f"Launched container {framework_config.container_name} {framework_container}"
                )
                # TODO this is gross, find a clever way
                time.sleep(5)
                for testname, commandlist in self.config.benchmarks[
                    "wrk_bench_tests"
                ].items():
                    logger.info(f"--|TEST: {testname}")
                    for command in commandlist.get("commands"):
                        logger.info(f"  |-- command: {command}")
                        (exit_code, output) = wrk_container.exec_run(cmd=command)
                        if exit_code == 0:
                            try:
                                pr = parsed_results(output.decode())
                                logger.debug(pr)
                            except Exception as e:
                                logger.error(e)
                                fn = (
                                    Path("tests/wrk_error_logs")
                                    / f"{str(uuid.uuid4())}.txt"
                                )
                                with open(fn, "w") as f:
                                    f.write(output.decode())
                        else:
                            logger.error(f"WRK ERROR: exit code is {exit_code}")
                            logger.error(f"WRK ERROR: output is {output}")

        except Exception as e:
            logger.error(f"error running: {e}")
        finally:
            cleant = self.cleanup()
            if not cleant:
                logger.error("something bad happened in cleaning")

    def start(
        self, container_config: CommonConfig, kill_if_up: bool = True
    ) -> Container:
        try:
            container = self.config.client.containers.get(
                container_config.container_name
            )
            if not kill_if_up:
                return container
            else:
                cleant_container = self.cleanup_containers(
                    container_config.container_name
                )
                assert cleant_container
                container = self._start(container_config=container_config)
                return container
        except NotFound as e:
            logger.error(e)
            container = self._start(container_config=container_config)
            return container

    def _start(self, container_config: CommonConfig) -> Container:
        command: Optional[str]
        if container_config.config is not None:
            wait_for_host = container_config.config.get("wait_for_host")
            wait_for_port = container_config.config.get("wait_for_port")
            command = f"sh -c './wait-for.sh {wait_for_host}:{wait_for_port} -t 10 -- {container_config.config['docker_cmd']}'"
        else:
            wait_for_host = None
            wait_for_port = None
            command = None

        if container_config.volume is not None:
            container = self.config.client.containers.run(
                container_config.image,
                name=container_config.container_name,
                command=command,
                network=self.config.network.name,
                detach=True,
                volumes={container_config.volume: {"bind": "/app", "mode": "rw"}},
            )
        else:
            container = self.config.client.containers.run(
                container_config.image,
                name=container_config.container_name,
                command=command,
                network=self.config.network.name,
                detach=True,
            )
        return container

    def cleanup_containers(
        self, container_name: Optional[str] = None, all: bool = False
    ) -> bool:
        try:
            containers_list = self.config.client.containers.list(all=True)
            for container in containers_list:
                if not all:
                    if container.name == container_name:
                        self.stop_container(container_name)
                        container.remove()
                        return True
                else:
                    if container.name in self.config.all_containers:
                        self.stop_container(all=True)
                        container.remove()
        except Exception as e:
            logger.error(e)
            return False
        return True

    def stop_container(
        self, container_name: Optional[str] = None, all: bool = False
    ) -> bool:
        try:
            containers_list = self.config.client.containers.list(all=True)
            for container in containers_list:
                if not all:
                    if container.name == container_name:
                        container.stop()
                        return True
                else:
                    if container.name in self.config.all_containers:
                        container.stop()
        except Exception as e:
            logger.error(e)
            return False
        return True

    def cleanup(self) -> bool:
        stopped_containers = self.stop_container(all=True)
        if stopped_containers:
            cleant_network = self.config.cleanup_network()
        else:
            cleant_network = False
            logger.error("Error stopping containers")
        cleant_containers = self.cleanup_containers(all=True)
        all_cleant = all([cleant_network, cleant_containers])
        return all_cleant


def main() -> None:
    config = Config()
    bench = Bfapi(config=config)
    bench.run()


if __name__ == "__main__":
    main()
