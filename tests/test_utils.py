from pathlib import Path

import pytest

from models.wrk import LatencyResult, NonXError, ReqPerSecResult, WrkError, WrkResult
from utils import parsed_results

data = [
    (
        "normal.txt",
        WrkResult(
            duration=2,
            Uduration="s",
            endpoint="http://bfapi.fastapi:8000/root",
            threads=2,
            connections=10,
            latencyResults=LatencyResult(
                latency_avg=3.65,
                Ulatency_avg="ms",
                latency_stdev=0.98,
                Ulatency_stdev="ms",
                latency_max=9.06,
                Ulatency_max="ms",
                latency_plusminus=81.88,
                Ulatency_plusminus="%",
            ),
            reqPerSecResults=ReqPerSecResult(
                rps_avg=1.36,
                Urps_avg="k",
                rps_stdev=109.26,
                Urps_stdev="",
                rps_max=1.51,
                Urps_max="k",
                rps_plusminus=80.00,
                Urps_plusminus="%",
            ),
            reqtot=5426,
            timetot=2.00,
            Utimetot="s",
            sizetot=757.73,
            Usizetot="KB",
            rps=2710.39,
            tps=378.50,
            Utps="KB",
        ),
    ),
    (
        "units.txt",
        WrkResult(
            duration=1,
            Uduration="m",
            endpoint="http://bfapi.fastapi:8000/root",
            threads=2,
            connections=10,
            latencyResults=LatencyResult(
                latency_avg=3.47,
                Ulatency_avg="ms",
                latency_stdev=0.99,
                Ulatency_stdev="ms",
                latency_max=13.75,
                Ulatency_max="ms",
                latency_plusminus=72.57,
                Ulatency_plusminus="%",
            ),
            reqPerSecResults=ReqPerSecResult(
                rps_avg=1.43,
                Urps_avg="k",
                rps_stdev=80.05,
                Urps_stdev="",
                rps_max=1.74,
                Urps_max="k",
                rps_plusminus=80.17,
                Urps_plusminus="%",
            ),
            reqtot=171109,
            timetot=1.00,
            Utimetot="m",
            sizetot=23.34,
            Usizetot="MB",
            rps=2850.11,
            tps=398.01,
            Utps="KB",
        ),
    ),
    (
        "timeout.txt",
        WrkResult(
            duration=10,
            Uduration="s",
            endpoint="http://bfapi.fastapi:8000/root",
            threads=10,
            connections=10000,
            latencyResults=LatencyResult(
                latency_avg=839.36,
                Ulatency_avg="ms",
                latency_stdev=71.76,
                Ulatency_stdev="ms",
                latency_max=940.64,
                Ulatency_max="ms",
                latency_plusminus=57.26,
                Ulatency_plusminus="%",
            ),
            reqPerSecResults=ReqPerSecResult(
                rps_avg=549.30,
                Urps_avg="",
                rps_stdev=612.80,
                Urps_stdev="",
                rps_max=2.48,
                Urps_max="k",
                rps_plusminus=82.08,
                Urps_plusminus="%",
            ),
            reqtot=25150,
            timetot=10.10,
            Utimetot="s",
            sizetot=3.43,
            Usizetot="MB",
            wrk_error=WrkError(connect=0, read=0, write=0, timeout=711),
            rps=2490.20,
            tps=347.75,
            Utps="KB",
        ),
    ),
    (
        "non200.txt",
        WrkResult(
            duration=10,
            Uduration="s",
            endpoint="http://bfapi.fastapi:8000/db1",
            threads=2,
            connections=200,
            latencyResults=LatencyResult(
                latency_avg=58.15,
                Ulatency_avg="ms",
                latency_stdev=2.98,
                Ulatency_stdev="ms",
                latency_max=126.70,
                Ulatency_max="ms",
                latency_plusminus=91.12,
                Ulatency_plusminus="%",
            ),
            reqPerSecResults=ReqPerSecResult(
                rps_avg=1.72,
                Urps_avg="k",
                rps_stdev=434.65,
                Urps_stdev="",
                rps_max=2.02,
                Urps_max="k",
                rps_plusminus=74.00,
                Urps_plusminus="%",
            ),
            reqtot=34295,
            timetot=10.08,
            Utimetot="s",
            sizetot=5.04,
            Usizetot="MB",
            wrk_error=NonXError(nonX=34295),
            rps=3403.01,
            tps=511.78,
            Utps="KB",
        ),
    ),
    (
        "stddev_unit.txt",
        WrkResult(
            duration=10,
            Uduration="s",
            endpoint="http://bfapi.fastapi:8000/root",
            threads=2,
            connections=1000,
            latencyResults=LatencyResult(
                latency_avg=333.75,
                Ulatency_avg="ms",
                latency_stdev=18.27,
                Ulatency_stdev="ms",
                latency_max=634.51,
                Ulatency_max="ms",
                latency_plusminus=90.30,
                Ulatency_plusminus="%",
            ),
            reqPerSecResults=ReqPerSecResult(
                rps_avg=1.66,
                Urps_avg="k",
                rps_stdev=0.90,
                Urps_stdev="k",
                rps_max=5.03,
                Urps_max="k",
                rps_plusminus=82.61,
                Urps_plusminus="%",
            ),
            reqtot=28947,
            timetot=10.01,
            Utimetot="s",
            sizetot=3.95,
            Usizetot="MB",
            rps=2892.33,
            tps=403.91,
            Utps="KB",
        ),
    ),
]


@pytest.mark.parametrize("filename, expected", data)
def test_parse(filename, expected, request):
    wrklogdir = Path(request.fspath).parent / "wrk_test_logs"
    fn = wrklogdir / filename
    assert fn.exists()
    with open(fn, "r") as wrklogfile:
        s = wrklogfile.read()
    pr = parsed_results(s)
    assert pr is not None
    assert pr == expected


def test_single_file(request):
    wrklogdir = Path(request.fspath).parent / "wrk_test_logs"
    fn = wrklogdir / "stddev_unit.txt"
    with open(fn, "r") as wrklogfile:
        s = wrklogfile.read()
    pr = parsed_results(s)
    assert pr is not None
