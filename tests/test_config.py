from main import Config
from models.bench import CommonConfig


def test_config(create_conf):
    config = Config(directory=create_conf)
    assert config.network.name == "benchnetwork"
    assert (
        config.benchmarks
        == {
            "wrk_bench_tests": {
                "bench_db1": {
                    "commands": [
                        "wrk -c 200 -d 10 -t 2 " "http://bfapi.fastapi:8000/db1",
                        "wrk -c 200 -d 10 -t 3 " "http://bfapi.fastapi:8000/db1",
                    ]
                },
                "bench_root": {
                    "commands": [
                        "wrk -c 1000 -d 10 -t 2 " "http://bfapi.fastapi:8000/root",
                        "wrk -c 1000 -d 10 -t 3 " "http://bfapi.fastapi:8000/root",
                    ]
                },
            }
        }
        != {
            "wrk_bench_tests": {
                "bench_route1": [
                    {
                        "wrk_commands": [
                            "wrk -c 1 -d 1 -t 1 " "HOST",
                            "wrk -c 2 -d 2 -t 2 " "HOST",
                        ]
                    }
                ]
            }
        }
    )

    assert "framework1" in config.frameworks.keys()
    assert isinstance(config.frameworks["framework1"], CommonConfig)
    assert isinstance(config.db, CommonConfig)
    assert isinstance(config.wrk, CommonConfig)
    for cont in ["bfapi.wrk", "bfapi.db", "bfapi.framework1"]:
        assert cont in config.all_containers
