from pathlib import Path

import pytest

BENCHTESTYAML = """
wrk_bench_tests:
  bench_root:
    commands:
      - wrk -c 1000 -d 10 -t 2 http://bfapi.fastapi:8000/root
      - wrk -c 1000 -d 10 -t 3 http://bfapi.fastapi:8000/root
  bench_db1:
    commands:
      - wrk -c 200 -d 10 -t 2 http://bfapi.fastapi:8000/db1
      - wrk -c 200 -d 10 -t 3 http://bfapi.fastapi:8000/db1
"""

DOCKERFILE = """
FROM python:3.7-buster
RUN apt update && apt-get install -y netcat
RUN pip install fastapi uvicorn databases[postgresql]
WORKDIR /app
"""

CONTAINER_CONFIG = """
docker_cmd: uvicorn main:app --host 0.0.0.0
use_volume: True
"""

DB_DOCKERFILE = """
FROM postgres:12.1
"""

WRK_DOCKERFILE = """
FROM debian:latest
RUN apt update && apt-get install -y build-essential libssl-dev git netcat
RUN git clone https://github.com/wg/wrk.git && cd wrk && make && mv wrk /usr/local/bin
WORKDIR /data
"""


@pytest.fixture(scope="session")
def create_conf(tmpdir_factory):
    layout = tmpdir_factory.mktemp("layout", numbered=False)
    with open(layout / "bench.yaml", "w") as f:
        f.write(BENCHTESTYAML)
    frameworks = layout.mkdir("frameworks")
    framework1 = frameworks.mkdir("framework1")
    with open(framework1 / "framework1.dockerfile", "w") as f:
        f.write(DOCKERFILE)
    with open(framework1 / "framework1.yaml", "w") as f:
        f.write(CONTAINER_CONFIG)
    common = layout.mkdir("common")
    db = common.mkdir("db")
    with open(db / "db.dockerfile", "w") as f:
        f.write(DB_DOCKERFILE)
    wrk = common.mkdir("wrk")
    with open(wrk / "wrk.dockerfile", "w") as f:
        f.write(WRK_DOCKERFILE)

    return Path(layout)
