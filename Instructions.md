# Run the benchmark

Still in a very alpha stage, just run `main.py`
This will:
1. build the containers, namely `bfapi.wrk` and `bfapi.db` containers located in the common folder 
    1. `bfapi.wrk` is the container that will run wrk against a FastAPI instance
    1. `bfapi.db` is the container that will put a db in operational mode,
2. build the containers located in the frameworks folder, more on that below
3. the script builds a config object from those 2 folders (see above) and from a `bench.yaml` file:
    1. `wrk_bench_tests` key contains the benchmarks names that will be run
    2. in each and every benchamrk name there is a `commands` key that lists all wrk commands the wrk container will run against all frameworks containers
    
    ```yaml
    wrk_bench_tests:
      bench_root:
        commands:
          - wrk -d 10 -c 64 --timeout 8 -t 32 http://bfapi.fastapi:8000/root
          - wrk -d 10 -c 128 --timeout 8 -t 32 http://bfapi.fastapi:8000/root
          - wrk -d 10 -c 256 --timeout 8 -t 32 http://bfapi.fastapi:8000/root
          - wrk -d 10 -c 512 --timeout 8 -t 32 http://bfapi.fastapi:8000/root
      bench_db1:
        commands:
          - wrk -d 10 -c 64 --timeout 8 -t 12 http://bfapi.fastapi:8000/root
          - wrk -d 10 -c 128 --timeout 8 -t 12 http://bfapi.fastapi:8000/root
          - wrk -d 10 -c 256 --timeout 8 -t 12 http://bfapi.fastapi:8000/root
          - wrk -d 10 -c 512 --timeout 8 -t 12 http://bfapi.fastapi:8000/root
    ```


# Add a new framework

You'll need a few things:
1. create a directory inside `frameworks` called `toto`
2. the dockerfile must be called `toto.dockerfile`
3. the container need to have `netcat` installed, this is to be sure we can wait for the db to be up before running the tests
4. the `wait-for.sh` needs to be inside the `toto` directory
5. there has to be a `toto.yaml` config file with those keys
    1. `docker_cmd` the command that will be run, ususally your webserver, uvicorn, gunicorn etc...
    2. `use_volume: True` if you want to use your `toto` directory as a volume, if you set it to False be sure your `toto.dockerfile` uses COPY your files
    3. `wait_for_host: bfapi.db` for that db host
    4. `wait_for_port: 5432` for the db port
        ```yaml
        docker_cmd: uvicorn main:app --host 0.0.0.0
        use_volume: True
        wait_for_host: bfapi.db
        wait_for_port: 5432
        ```