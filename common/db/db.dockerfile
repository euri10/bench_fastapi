FROM postgres:12.1
RUN apt-get update \
    && apt-get install --no-install-recommends -y postgresql-12-ip4r postgresql-12-hll \
    && rm -rf /var/lib/apt/lists/*
ADD init.sql /docker-entrypoint-initdb.d

COPY ./data/f1db.dump /data/f1db.dump

