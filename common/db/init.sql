create extension if not exists "uuid-ossp";
create extension if not exists "btree_gist";
create extension if not exists "ip4r";
create extension if not exists "hll";
create extension if not exists "cube";
create extension if not exists "earthdistance";
create extension if not exists "hstore";
create extension if not exists "intarray";
create extension if not exists "pg_trgm";

create database f1db;