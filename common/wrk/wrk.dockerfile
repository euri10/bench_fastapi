FROM debian:latest

RUN apt update && apt-get install -y build-essential libssl-dev git netcat
RUN git clone https://github.com/wg/wrk.git && cd wrk && make && mv wrk /usr/local/bin
WORKDIR /data
#sh -c "while true; do sleep 1; done"
CMD ["/bin/sh", "-c", "while true; do sleep 1; done"]