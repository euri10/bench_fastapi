import logging
import re
from typing import Optional, Union

from models.wrk import LatencyResult, NonXError, ReqPerSecResult, WrkError, WrkResult

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def parsed_results(s: str) -> Optional[WrkResult]:
    patterns = [
        r"Running (?P<duration>\d+)(?P<Uduration>(s|m)) test @ (?P<endpoint>.*)",
        r"\s+(?P<threads>\d+) threads and (?P<connections>\d+) connections",
        r"\s+Thread Stats\s+Avg\s+Stdev\s+Max\s+\+\/- Stdev",
        r"\s+Latency\s+(?P<latency_avg>\d+\.\d+)(?P<Ulatency_avg>(s|ms))\s+(?P<latency_stdev>\d+\.\d+)(?P<Ulatency_stdev>(s|ms))\s+(?P<latency_max>\d+\.\d+)(?P<Ulatency_max>(s|ms))\s+(?P<latency_plusminus>\d+\.\d+)(?P<Ulatency_plusminus>%)",
        r"\s+Req/Sec\s+(?P<rps_avg>\d+\.\d+)(?P<Urps_avg>(k|))\s+(?P<rps_stdev>\d+\.\d+)(?P<Urps_stdev>(k|))\s+(?P<rps_max>\d+\.\d+)(?P<Urps_max>(k|))\s+(?P<rps_plusminus>\d+\.\d+)(?P<Urps_plusminus>%)",
        r"\s+(?P<reqtot>\d+) requests in (?P<timetot>\d+\.\d+)(?P<Utimetot>(s|m)), (?P<sizetot>\d+\.\d+)(?P<Usizetot>(K|M)B) read",
        r"Requests/sec:\s+(?P<rps>\d+\.\d+)",
        r"Transfer/sec:\s+(?P<tps>\d+\.\d+)(?P<Utps>(K|M)B)",
    ]
    patterns_opt = [
        r"\s+Socket errors: connect (?P<connect>\d+), read (?P<read>\d+), write (?P<write>\d+), timeout (?P<timeout>\d+)",
        r"\s+Non-2xx or 3xx responses: (?P<nonX>\d+)",
    ]
    matches = []
    if len(s.splitlines()) == 9:
        if re.findall(patterns_opt[0], s):
            patterns.insert(6, patterns_opt[0])
        elif re.findall(patterns_opt[1], s):
            patterns.insert(6, patterns_opt[1])

    for index, line in enumerate(s.splitlines()):
        try:
            m = re.match(patterns[index], line)
            matches.append(m)
        except Exception as e:
            logger.error(e)

    PATTERN = "\n".join(patterns)
    m = re.match(PATTERN, s)

    if m is not None:
        latency_results = LatencyResult(
            latency_avg=m.group("latency_avg"),
            Ulatency_avg=m.group("Ulatency_avg"),
            latency_stdev=m.group("latency_stdev"),
            Ulatency_stdev=m.group("Ulatency_stdev"),
            latency_max=m.group("latency_max"),
            Ulatency_max=m.group("Ulatency_max"),
            latency_plusminus=m.group("latency_plusminus"),
            Ulatency_plusminus=m.group("Ulatency_plusminus"),
        )
        req_per_sec_results = ReqPerSecResult(
            rps_avg=m.group("rps_avg"),
            Urps_avg=m.group("Urps_avg"),
            rps_stdev=m.group("rps_stdev"),
            Urps_stdev=m.group("Urps_stdev"),
            rps_max=m.group("rps_max"),
            Urps_max=m.group("Urps_max"),
            rps_plusminus=m.group("rps_plusminus"),
            Urps_plusminus=m.group("Urps_plusminus"),
        )
        wrk_error: Optional[Union[WrkError, NonXError]]
        if len(s.splitlines()) == 8:
            wrk_error = None
        else:
            if patterns[6] == patterns_opt[0]:
                wrk_error = WrkError(
                    connect=m.group("connect"),
                    read=m.group("read"),
                    write=m.group("write"),
                    timeout=m.group("timeout"),
                )
            elif patterns[6] == patterns_opt[1]:
                wrk_error = NonXError(nonX=m.group("nonX"))
        wrk_result = WrkResult(
            duration=m.group("duration"),
            Uduration=m.group("Uduration"),
            endpoint=m.group("endpoint"),
            threads=m.group("threads"),
            connections=m.group("connections"),
            latencyResults=latency_results,
            reqPerSecResults=req_per_sec_results,
            reqtot=m.group("reqtot"),
            timetot=m.group("timetot"),
            Utimetot=m.group("Utimetot"),
            sizetot=m.group("sizetot"),
            Usizetot=m.group("Usizetot"),
            wrk_error=wrk_error,
            rps=m.group("rps"),
            tps=m.group("tps"),
            Utps=m.group("Utps"),
        )
        return wrk_result
    else:
        return None
